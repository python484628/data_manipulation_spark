# -*- coding: utf-8 -*-


# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# October 2021

# Install library if necessary
!pip install pyspark

# Start Spark session
from pyspark.sql import SparkSession
from pyspark.sql.functions import *

spark = SparkSession.builder.master("local[*]").appName("Covid App").config("spark.executor.memory","1g").getOrCreate()

# Define path
path = "ecdc-covid19_latest.csv"

# Download file
!pip install wget 
import wget
url = "https://opendata.ecdc.europa.eu/covid19/casedistribution/csv"
wget.download(url,path)

# Store data in covid variable and vizualize the table with show()
covid = spark.read.csv(path, inferSchema = True, header = True, sep = ',')

covid.show()

# Print columns of our covid table
covid.printSchema()

# Number of records for France only
print(covid.filter(covid["countriesAndTerritories"]== "France").count()) # 350

# Number of records for each country and vizualize it in a table
per_country = covid.groupBy("continentEXP","countriesAndTerritories").count().orderBy("count")

per_country.show()

# Number of records for each country that have minimum 150 records
per_country.filter(per_country["count"] >= 150).count()

# Number of records for each continent and vizualize it in a table
per_continent = covid.groupBy("continentEXP").count().sort(desc("count"))

per_continent.show()

# Number of cases for each continent and vizualize it in a table
case_cont = covid.groupBy("continentEXP").agg(sum("cases").alias("NbCas")).sort(desc("NbCas"))

case_cont.show()

# Number of records, total cases and mean cases per day for each continent and vizualize it
 covid.groupBy("continentEXP")\
  .agg(sum("cases").alias("NbCases"), \
       count("cases").alias("records"),\
       avg("cases").alias("MeanCases")).\
       sort(desc("NbCases")).show()

# Top-10 countries that have the most number of cases and vizualize it
covid.groupBy("countriesAndTerritories").agg(sum("cases").alias("NbCas")).sort(desc("NbCas")).show(10)

# Top-10 countries that have the most number of deaths and vizualize it
covid.groupBy("continentExp","countriesAndTerritories").agg(sum("deaths").alias("NbDeces")).sort(desc("NbDeces")).show(10)

# Top-10 countries that have the most number of deaths (in average) and vizualize it
covid.groupBy("continentExp","countriesAndTerritories").agg(avg("deaths").alias("mean_deces_per_day")).sort(desc("mean_deces_per_day")).show(10)

# Import library to manipulate time
from datetime import date, timedelta

# Collect max dates in our covid table and print the minimum date (calculated by max date - 5 days)
cov_date = covid.select("deaths", "countriesAndTerritories", to_date("dateRep","dd/mm/yyyy").alias("date"))

max_date = cov_date.select(max(cov_date.date)).first()["max(date)"]

date_min = max_date + timedelta(days=-5)

print(max_date)
print(date_min)

# Number of deaths and mean deaths above the minimum date for the top-10 countries and vizualize it in a table
cov_date.filter(cov_date.date >= date_min).groupBy("countriesAndTerritories").agg(count("deaths").alias("deaths"),avg("deaths").alias("mean_deaths")).orderBy(desc("mean_deaths")).show(10)

# Calculate total number of deaths and the average of deaths per day between 2020-01-26 and 2020-01-31 (max date), and vizualize it in a table
from datetime import date, timedelta
a = covid.select(max(to_date(covid.dateRep, 'dd/MM/yyyy'))).first() 
until_date = list(a.asDict().values())[0] + timedelta(days = -5)

covid.filter(to_date(covid['dateRep'], 'dd/MM/yyyy') >= until_date) \
    .groupBy('ContinentEXP', 'countriesAndTerritories') \
    .agg(count('deaths').alias('deaths'), sum('deaths').alias('total deaths'), round(mean('deaths'), 2).alias("Mean_Deaths_per_day")) \
    .orderBy(desc('Mean_Deaths_per_day')).show(10)

# Calculate if there is a correlation between France and Italy in terms of deaths
fr_it = covid.filter((covid.countriesAndTerritories == 'France') | (covid.countriesAndTerritories == 'Italy'))\
.groupBy("dateRep").pivot("countriesAndTerritories").sum("deaths")

fr_it.stat.corr('France','Italy')
# 0.74, so deaths between France and Italy look correlated (not completely because not equals to 1)

# Calculate if there is a correlation between Spain and Italy in terms of deaths
esp_it = covid.filter((covid.countriesAndTerritories == 'Spain') | (covid.countriesAndTerritories == 'Italy'))\
.groupBy("dateRep").pivot("countriesAndTerritories").sum("deaths")


esp_it.stat.corr('Spain','Italy')
# 0.61 so deaths between Spain and Italy look correlated but less than France and Italy together

# Calculate if there is a correlation between USA and Brazil in terms of deaths
usa_bra = covid.filter((covid.countriesAndTerritories == 'United_States_of_America') | (covid.countriesAndTerritories == 'Brazil'))\
.groupBy("dateRep").pivot("countriesAndTerritories").sum("deaths")

usa_bra.stat.corr('United_States_of_America','Brazil')

# 0.41 so it seems correlated but weakly

# Calculate number of deaths for specific countries : France,, Italy, USA, Brazil and Spain and vizualize it in a table
pivot_data = covid.filter((covid.countriesAndTerritories == 'France') | (covid.countriesAndTerritories == 'Italy') | (covid.countriesAndTerritories == 'United_States_of_America') | (covid.countriesAndTerritories == 'Brazil') |(covid.countriesAndTerritories == 'Spain') )\
.groupBy("dateRep").pivot("countriesAndTerritories").sum("deaths")


pivot_data.show()

# Stop spark context
spark.stop()
